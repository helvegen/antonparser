<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    private const DATA = [
        ['title' => 'Авторский текст', 'code' => 'text', 'is_category' => false],
        ['title' => 'Новости', 'code' => 'news', 'is_category' => false],
    ];

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $table = DB::table('sections');
        foreach (self::DATA as $section) {
            $table->insert($section);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::table('sections')
            ->whereIn('code', array_column(self::DATA, 'code'))
            ->delete();
    }
};
