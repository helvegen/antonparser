<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    private const DATA = [
        ['title' => 'Аутизм', 'code' => null],
        ['title' => 'Родителям', 'code' => 'for_parents'],
        ['title' => 'Специалистам', 'code' => 'for_specialists'],
        ['title' => 'Аутичным людям', 'code' => 'for_autistic_people'],
    ];

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $table = DB::table('sections');
        foreach (self::DATA as $section) {
            $table->insert($section);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::table('sections')->truncate();
    }
};
