$(document).ready(function () {
    // modals
    $('li.blocks-gallery-item').click(function () {
        let src = $(this).find('img').attr('src');
        $('div.modal-content').css('backgroundImage', 'url("' + src + '")');
        $('div.modal-bg').css('display', 'flex');
    })
    $('div.modal-bg').click(function () {
        $(this).hide();
    })
    // modals - end

    // scroll up
    $(window).scroll(function() {
        let buttonUp = $('button.arrow-up');
        if ($(window).scrollTop() > 500) {
            buttonUp.fadeIn(500);
        } else {
            buttonUp.fadeOut(500);
        }
    })
    $('button.arrow-up').click(function () {
        $('body,html').animate({scrollTop: 0}, 500);
    })
    // scroll up - end
})
