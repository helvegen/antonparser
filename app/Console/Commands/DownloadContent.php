<?php

namespace App\Console\Commands;

use App\Facades\Parser;
use App\Models\Article;
use App\Models\Section;
use Exception;
use Illuminate\Console\Command;
use Symfony\Component\Console\Command\Command as CommandAlias;
use Throwable;

class DownloadContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:download-content {category? : Category code from query parameter}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download content from single article or category';

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $categoryCode = $this->argument('category');
        if (is_null($categoryCode)) {
            $articles = $this->buildSingleArticle();
        } else {
            $articles = $this->buildArticles($categoryCode);
        }

        $savedCount = 0;
        /** @var Article $article */
        foreach ($articles as $article) {
            try {
                Article::where('url', $article->url)
                    ->firstOr(function () use ($article, &$savedCount) {
                        if ($article->save()) {
                            $savedCount++;
                        }
                    });
            } catch (Throwable $e) {
                $this->error($e->getMessage());
            }
        }

        $this->info("Articles saved: $savedCount");

        return CommandAlias::SUCCESS;
    }

    private function buildSingleArticle(): array
    {
        $articles = [];

        try {
            $articles[] = Parser::getArticle();
        } catch (Exception $e) {
            $this->error($e->getMessage());
        }

        return $articles;
    }

    private function buildArticles(string $categoryCode): array
    {
        try {
            $section = Section::where('code', $categoryCode)->firstOrFail();
            if (is_null($section)) {
                $this->error('Wrong section code');

                return [];
            }
        } catch (Exception $e) {
            $this->error($e->getMessage());

            return [];
        }

        $articles = [];
        $links = Parser::getLinks($categoryCode);
        krsort($links);

        $this->info("Start downloading from category $categoryCode");
        $bar = $this->output->createProgressBar(count($links));
        $bar->start();

        foreach ($links as $link) {
            try {
                $articles[] = Parser::getArticle($link, $section->id);
            } catch (Exception $e) {
                $this->error($e->getMessage());
            }
            $bar->advance();
        }

        $bar->finish();
        $this->newLine();

        return $articles;
    }
}
