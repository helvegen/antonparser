<?php

namespace App\Console\Commands;

use App\Facades\Parser;
use App\Models\Article;
use App\Models\Section;
use Illuminate\Console\Command;
use Symfony\Component\Console\Command\Command as CommandAlias;

class DownloadAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:download-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download content from all known sections';

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $savedSections = 0;
        $sections = Section::get();

        /** @var Section $section */
        foreach ($sections as $section) {
            $result = $this->call(DownloadContent::class, ['category' => $section->code]);
            if ($result === CommandAlias::SUCCESS) {
                $savedSections++;
            }
        }

        $this->info("Sections viewed: $savedSections");

        $this->relink();

        return CommandAlias::SUCCESS;
    }

    private function relink(): void
    {
        $this->info('Start relinking');

        $articles = Article::all();

        $bar = $this->output->createProgressBar(count($articles));
        $bar->start();

        foreach ($articles as $article) {
            $relinkedArticle = Parser::relink($article);
            $relinkedArticle->save();
            $bar->advance();
        }

        $bar->finish();
        $this->newLine();
    }
}
