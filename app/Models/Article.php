<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string title
 * @property int section_id
 * @property string url
 * @property string text
 *
 * @method static where(string $column, mixed $value)
 */
class Article extends Model
{
    use HasFactory;
}
