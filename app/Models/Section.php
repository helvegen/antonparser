<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string title
 * @property string code
 * @property boolean is_category
 *
 * @method static findOrFail(int $sectionId)
 * @method static get(): Collection
 * @method static where(string $column, mixed $value)
 */
class Section extends Model
{
    use HasFactory;
}
