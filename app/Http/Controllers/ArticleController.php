<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;

class ArticleController extends Controller
{
    public function showAction(int $articleId): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('article', ['article' => Article::findOrFail($articleId)]);
    }
}
