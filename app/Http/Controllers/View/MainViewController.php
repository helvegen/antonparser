<?php

namespace App\Http\Controllers\View;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SectionController;
use App\Models\Article;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;

class MainViewController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(
        Request $request,
        SectionController $sectionController
    ): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('layouts.main', [
            'sections' => $sectionController->list(),
            'article' => Article::where('section_id', 1)
                ->orderBy('id', 'DESC')
                ->first(),
        ]);
    }
}
