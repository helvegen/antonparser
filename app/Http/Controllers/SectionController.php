<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Section;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Application;
use Illuminate\Contracts\Foundation\Application as AppContract;

class SectionController extends Controller
{
    public function list(): Collection
    {
        return Section::all();
    }

    public function showAction(int $sectionId): View|Application|Factory|AppContract
    {
        return view('section', [
            'section' => Section::findOrFail($sectionId),
            'articles' => Article::where('section_id', $sectionId)
                ->orderByDesc('id')
                ->get()
        ]);
    }
}
