<?php

namespace App\Facades;

use App\Models\Article;
use Illuminate\Support\Facades\Facade;

/**
 * @method static getArticle(?string $url = null, int $sectionId = 1): App\Models\Article
 * @method static getLinks(string $sectionCode): string[]
 * @method static relink(Article $article): Article
 */
class Parser extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'parser';
    }
}
