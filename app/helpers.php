<?php

if (!function_exists('url_ext')) {
    function url_ext(string $path, array $params = []): string
    {
        return sprintf(
            '%s?%s',
            $path,
            http_build_query($params),
        );
    }
}
