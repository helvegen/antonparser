<?php

namespace App\Enums;

enum HTML_Selector: string
{
    case POST = 'article.post';
    case TITLE = 'h1';
    case LINK = 'a.post';
    case POST_HREF = 'a.post__href';
    case NEWS_ITEM = 'div.news-item';
}
