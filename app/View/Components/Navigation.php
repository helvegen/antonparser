<?php

namespace App\View\Components;

use App\Http\Controllers\SectionController;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class Navigation extends Component
{
    public Collection $sections;

    /**
     * Create a new component instance.
     */
    public function __construct(
        SectionController $sectionController,
    )
    {
        $this->sections = $sectionController->list();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.navigation', [
            'appUrl' => config('app.url'),
            'currentUrl' => url()->current(),
            'previousUrl' => url()->previous(),
        ]);
    }
}
