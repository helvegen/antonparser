<?php

namespace App\Services;

use App\Models\Article;
use App\Models\Section;
use Exception;
use InvalidArgumentException;
use IvoPetkov\HTML5DOMDocument;
use IvoPetkov\HTML5DOMElement;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Enums\HTML_Selector;

use function url_ext;

class Parser
{
    private string $baseUrl;
    private string $domenUrl;
    private HTML5DOMDocument $dom;

    private const STR_SEARCH = [
        '<a href',
        'loading="lazy"',
        '<a class',
        '<p></p>',
        'style="background-color:#cccccc"',
        '<p><br>',
        'figure',
        'wp-block-button__link has-black-background-color has-background no-border-radius',
        '<p>&lt;…&gt;</p>',
        'wp-block-columns',
    ];
    private const STR_REPLACE = [
        '<a target="_blank" href',
        '',
        '<a target="_blank" class',
        '',
        '',
        '<p>',
        'div',
        'btn btn-sm btn-success text-light mb-3',
        '',
        'wp-block-columns d-flex my-5',
    ];
    private const TO_REMOVE = [
        'div.wp-block-spacer',
        'div.wp-block-jetpack-tiled-gallery',
        'figcaption',
        'div.wp-block-cover',
        'figure.size-large',
    ];

    public function __construct()
    {
        $config = config('services.parser');
        $this->baseUrl = $config['baseUrl'];
        $this->domenUrl = $config['domenUrl'];
        $this->dom = new HTML5DOMDocument();
    }

    /**
     * @throws Exception
     */
    public function getArticle(?string $url = null, int $sectionId = 1): Article
    {
        $section = Section::findOrFail($sectionId);
        if (is_null($section)) {
            throw new InvalidArgumentException('Wrong section id');
        }

        $url = $url ?? $this->baseUrl;

        $html = file_get_contents($url);
        if ($html === false) {
            throw new NotFoundHttpException('Requested page not found');
        }

        $this->dom->loadHTML($html);

        $titleNode = $this->dom->querySelector(HTML_Selector::TITLE->value);
        $titleNode->parentNode->removeChild($titleNode);

        $this->cleanUpHtml();
        $this->downloadImg($section->id);

        $article = new Article();
        $article->title = $titleNode->innerHTML;
        $article->section_id = $sectionId;
        $article->url = $url;
        $article->text = str_replace(
            self::STR_SEARCH,
            self::STR_REPLACE,
            $this->dom->querySelector(HTML_Selector::POST->value)->outerHTML,
        );

        return $article;
    }

    /**
     * @throws Exception
     * @returns string[]
     */
    public function getLinks(string $sectionCode): array
    {
        $section = Section::where('code', $sectionCode)->first();
        if (is_null($section)) {
            throw new InvalidArgumentException('Wrong section code');
        }

        if ($section->is_category) {
            $url = url_ext($this->baseUrl, ['category' => $sectionCode]);
            $selectorEnum = HTML_Selector::LINK;
        } else {
            $url = "{$this->domenUrl}/$sectionCode/";
            $selectorEnum = HTML_Selector::POST_HREF;
        }

        $html = file_get_contents($url);
        if ($html === false) {
            throw new NotFoundHttpException('Requested page not found');
        }
        $this->dom->loadHTML($html);

        $linkNodes = $this->dom->querySelectorAll($selectorEnum->value);
        $links = [];

        foreach ($linkNodes as $linkNode) {
            $links[] = $linkNode->getAttribute('href');
        }

        if (empty($links)) {
            $this->getLinksFromNewsItems($links);
        }

        return $links;
    }

    /**
     * @throws Exception
     */
    public function relink(Article $article): Article
    {
        $this->dom->loadHTML($article->text);
        $links = $this->dom->querySelectorAll("a[href^=\"{$this->domenUrl}\"]");

        /** @var HTML5DOMElement $link */
        foreach ($links as &$link) {
            $href = $link->getAttribute('href');
            $hrefParts = explode('/', trim($href, '/'));
            $articleCode = end($hrefParts);
            $ownArticle = Article::whereRaw('url LIKE ?', "%$articleCode/")->first();
            if (!is_null($ownArticle)) {
                $ownHref = route('article', ['articleId' => $ownArticle->id]);
                $link->setAttribute('href', $ownHref);
                $link->removeAttribute('target');
                $article->text = $this->dom->saveHTML();
            }
        }

        return $article;
    }

    private function getLinksFromNewsItems(array &$links): void
    {
        $newsItems = $this->dom->querySelectorAll(HTML_Selector::NEWS_ITEM->value);
        /** @var HTML5DOMElement $newsItem */
        foreach ($newsItems as $newsItem) {
            $links[] = $newsItem->firstElementChild->getAttribute('href');
        }
    }

    private function cleanUpHtml(): void
    {
        try {
            $nodesToRemove = $this->dom->querySelectorAll(
                implode(', ', self::TO_REMOVE)
            );

            /** @var HTML5DOMElement $nodeToRemove */
            foreach ($nodesToRemove as $nodeToRemove) {
                $nodeToRemove->remove();
            }
        } catch (\Throwable) {}
    }

    private function downloadImg(int $sectionId): void
    {
        $imgNodes = $this->dom->querySelectorAll('img');
        $dirPath = config('app.imgPath') . $sectionId;
        if (!empty($imgNodes) && !is_dir($dirPath)) {
            mkdir($dirPath, recursive: true);
        }

        /** @var HTML5DOMElement $imgNode */
        foreach ($imgNodes as &$imgNode) {
            $src = $imgNode->getAttribute('src');
            $fileNameArray = explode('/', $src);
            $filePath = $dirPath . DIRECTORY_SEPARATOR . end($fileNameArray);
            if (($img = file_get_contents($src)) !== false) {
                if (file_put_contents($filePath, $img) !== false) {
                    $imgNode->setAttribute('src', $filePath);
                }
            }
        }
    }
}
