@extends('layouts.main')
@section('title', config('app.name') . ': '. html_entity_decode($section->title))

@section('content')
    @if(count($articles) === 1)
        <x-article :article="$articles[0]"/>
    @else
        <h1 class="text-center py-4">{{ $section->title }}</h1>
        @if(empty($articles->toArray()))
            <div class="alert alert-light mx-5" role="alert">
                В этом разделе пока ничего нет
            </div>
        @else
            <div class="d-flex flex-wrap justify-content-between">
                @foreach($articles as $article)
                    <a href="{{ route('article', ['articleId' => $article->id]) }}"
                       class="card my-3 mx-3 section-card border-success w-25 flex-grow-1"
                    >
                        <div class="card-body">
                            <h5 class="card-title">{{ html_entity_decode($article->title) }}</h5>
                            <h6 class="card-subtitle my-2 text-muted fw-lighter">
                                {{ date('d.m.Y', strtotime($article->created_at)) }}
                            </h6>
                            <p class="card-text">
                                {{ Str::words(strip_tags(html_entity_decode($article->text)), 15)}}
                            </p>
                        </div>
                    </a>
                @endforeach
            </div>
        @endif
    @endif
@endsection
