<div>
    <h1 class="text-center py-4">
        {{ html_entity_decode($article->title) }}
    </h1>
    <div class="px-4">
        {!! $article->text !!}
    </div>
    @if(!is_null($article->url) && $article->url !== config('services.parser.baseUrl'))
        <div class="p-3 pt-5">
            <a href="{{ url()->previous() }}" class="btn btn-sm btn-success">
                &larr;&nbsp;Назад
            </a>
            <a href="{{ $article->url }}" class="btn btn-sm btn-success" target="_blank">
                Оригинальный текст&nbsp;&rarr;
            </a>
        </div>
    @endif
</div>
