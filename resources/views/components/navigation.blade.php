@php
    $classActive = 'btn-light';
    $class = 'btn-outline-light';
@endphp

<nav class="ap-navbar">
    <div class="d-flex flex-wrap m-0 p-3 justify-content-between">
        <div class="me-4">
            <a href="{{ $appUrl }}"
               class="btn btn-sm {{ $appUrl == $currentUrl ? $classActive : $class }}"
            >
                Главная
            </a>
        </div>
        <div class="d-flex flex-wrap justify-content-end">
            @if(!empty($previousUrl))
                <a href="{{ $previousUrl }}" class="btn btn-sm text-light">
                    &larr;&nbsp;Назад
                </a>
            @endif
            @foreach($sections as $section)
                @php($routeUrl = route('section', ['sectionId' => $section->id]))
                    <a href="{{ $routeUrl }}"
                       class="btn btn-sm {{ $routeUrl == $currentUrl ? $classActive : $class }} ms-3"
                    >
                        {{ $section->title }}
                    </a>
            @endforeach
        </div>
    </div>
</nav>
