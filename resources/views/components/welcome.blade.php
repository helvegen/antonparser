<div class="welcome text-center">
    <h1>Привет!</h1>
    <p>
        Этот сайт - зеркало самого популярного в Рунете ресурса по РАС,
        <a href="{{ config('services.parser.domenUrl') }}" target="_blank">Антон тут рядом</a>.
    </p>
    <p>
        Благотворительный фонд «Антон тут рядом» — фонд системной поддержки людей с аутизмом,
        основанный в 2013 году в Санкт-Петербурге.
    </p>
    <p>
        К сожалению, многим моим знакомым сложно ориентироваться в ярком и сочном дизайне сайта.
        Я хочу популяризировать тему РАС и помочь им получить доступ к полезной информации,
        поэтому сделала новое оформление в спокойных цветах.
    </p>
    <p>
        В каждой статье внизу страницы есть ссылка на первоисточник.
    </p>
    <p>Спасибо!</p>

    @if(isset($sections))
        <div class="d-flex flex-wrap justify-content-between my-5">
            @foreach($sections as $section)
                <a href="{{ route('section', ['sectionId' => $section->id]) }}"
                   class="card my-3 mx-3 section-card border-success w-25 flex-grow-1"
                >
                    <div class="card-body">
                        <h5 class="card-title">{{ html_entity_decode($section->title) }}</h5>
                    </div>
                </a>
            @endforeach
        </div>
    @endif
</div>
