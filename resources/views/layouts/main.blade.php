<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @yield('title', config('app.name'))
    </title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset("/build/css/app.css") }}">
</head>
<body>
<div class="wrapper bg-success-subtle">
    <section class="navigation">
        <x-navigation/>
    </section>
    <section class="content mb-4">
        @section('content')
            @if(isset($sections))
                <x-welcome :sections="$sections"/>
            @endif
        @show
    </section>
    <footer class="bg-success text-light text-center py-3">
        <a href="https://antontut.ru/" class="text-light" target="_blank">Антон тут рядом</a>,&nbsp;
        <span>{{ date('Y') }}</span>
    </footer>
    <button class="arrow-up btn btn-success btn-lg" title="Наверх">&#9650;</button>
</div>

<script src="{{ asset('/build/js/jquery.min.js') }}"></script>
<script src="{{ asset("/build/js/app.js") }}"></script>

</body>
</html>
