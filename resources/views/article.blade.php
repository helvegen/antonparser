@extends('layouts.main')
@section('title', config('app.name') . ': '. html_entity_decode($article->title))

@section('content')
    <x-article :article="$article" />
    <div class="modal-bg">
        <div class="modal-inner">
            <div class="modal-content"></div>
        </div>
    </div>
@endsection
